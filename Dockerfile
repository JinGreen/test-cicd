# Используем базовый образ
FROM alpine:latest

# Устанавливаем обновления и полезные утилиты
RUN apk update && apk add --no-cache curl

# Создаем файл с сообщением
RUN echo "Hello, Kaniko!!!" > /hello.txt

# Указываем команду по умолчанию
CMD ["cat", "/hello.txt"]